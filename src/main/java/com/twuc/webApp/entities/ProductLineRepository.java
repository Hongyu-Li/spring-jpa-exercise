package com.twuc.webApp.entities;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine,String> {
    Optional<ProductLine> findByProductLine(String productLine);
    Page<ProductLine> findAll(Pageable pageable);
}

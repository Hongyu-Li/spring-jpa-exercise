package com.twuc.webApp.web;

import com.twuc.webApp.contract.DoesNotExistException;
import com.twuc.webApp.contract.GetProductRequest;
import com.twuc.webApp.entities.Product;
import com.twuc.webApp.entities.ProductLine;
import com.twuc.webApp.entities.ProductLineRepository;
import com.twuc.webApp.entities.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductLineRepository productLineRepository;

    @GetMapping("/products/{id}")
    public ResponseEntity getProduct(@PathVariable String id){
        final Product product = productRepository.findProductByProductCode(id).orElseThrow(DoesNotExistException::new);
        return ResponseEntity.status(200).body(product);
    }

    @GetMapping("/product-lines/{id}")
    public ResponseEntity getProductLine(@PathVariable String id){
        final ProductLine productLine = productLineRepository.findByProductLine(id).orElseThrow(DoesNotExistException::new);
        return ResponseEntity.status(200).body(productLine);
    }

    @GetMapping("/products")
    public ResponseEntity getAllProducts(@RequestParam(defaultValue = "0",required = false) Integer page,
                                         @RequestParam(defaultValue = "20",required = false) Integer size,
                                         @RequestParam(defaultValue = "productCode",required = false) String sort,
                                         @RequestParam(defaultValue = "ASC",required = false) Sort.Direction direction){
        final Page<Product> allProduct = productRepository.findAll(PageRequest.of(page,size,Sort.by(direction,sort)));
        return ResponseEntity.status(200).body(allProduct.get().toArray());
    }

    @GetMapping("/product-lines")
    public ResponseEntity getAllProductLines(@RequestParam(defaultValue = "0",required = false) Integer page,
                                         @RequestParam(defaultValue = "20",required = false) Integer size,
                                         @RequestParam(defaultValue = "productLine",required = false) String sort,
                                         @RequestParam(defaultValue = "ASC",required = false) Sort.Direction direction){
        final Page<ProductLine> allProductLines = productLineRepository.findAll(PageRequest.of(page,size,Sort.by(direction,sort)));
        return ResponseEntity.status(200).body(allProductLines.get().toArray());
    }

    @PostMapping("/products")
    public ResponseEntity createProducts(@RequestBody GetProductRequest request){
        if (productRepository.findProductByProductCode(request.getProductCode()).isPresent()){
            return ResponseEntity.status(404).body(request.getProductCode() + " already exist");
        }
        productRepository.saveAndFlush(new Product(request.getProductCode(),request.getProductName(),request.getProductLine(),
                request.getProductScale(),request.getProductVendor(),request.getProductDescription(),request.getQuantityInStock(),
                request.getBuyPrice(),request.getMSRP()));
        return ResponseEntity.status(201).build();
    }


}

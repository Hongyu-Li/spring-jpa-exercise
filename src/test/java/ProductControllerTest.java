import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.GetProductRequest;
import com.twuc.webApp.entities.Product;
import com.twuc.webApp.entities.ProductLine;
import com.twuc.webApp.entities.ProductLineRepository;
import com.twuc.webApp.entities.ProductRepository;
import com.twuc.webApp.web.ApiTestBase;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest extends ApiTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductLineRepository productLineRepository;

    @Autowired
    private EntityManager em;

    @Test
    void shouldReturn200WhenGetProduct() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products/S10_1678"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.productCode", Matchers.is("S10_1678")));
    }

    @Test
    void shouldReturn200WhenGetProductLine() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines/Motorcycles"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.productLine", Matchers.is("Motorcycles")));
    }

    @Test
    void shouldReturn404WhenProductDoesNotExist() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products/2"))
                .andExpect(status().is(404));
    }

    @Test
    void shouldReturn404WhenProductLineDoesNotExist() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines/>_<"))
                .andExpect(status().is(404));
    }

    @Test
    void shouldGetAllProducts() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productCode",Matchers.is("S10_1678")))
                .andExpect(jsonPath("$[1].productCode",Matchers.is("S10_1679")))
                .andExpect(jsonPath("$[2].productCode",Matchers.is("S10_1680")));
    }

    @Test
    void shouldGetAllProductsDESC() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products?direction=DESC"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[2].productCode",Matchers.is("S10_1678")))
                .andExpect(jsonPath("$[1].productCode",Matchers.is("S10_1679")))
                .andExpect(jsonPath("$[0].productCode",Matchers.is("S10_1680")));
    }

    @Test
    void shouldGetAllProductsSplitShow() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products?page=0&&size=1"))
                .andExpect(status().is(200));

        Page<Product> pages = productRepository.findAll(
                PageRequest.of(1, 2, Sort.by(Sort.Direction.ASC, "productCode")));
        assertEquals(2,pages.getTotalPages());
        assertEquals(2,pages.getSize());
        assertArrayEquals(
                new String[] { "S10_1680" },
                pages.stream().map(Product::getProductCode).toArray(String[]::new));
    }

    @Test
    void shouldGetAllProductsSplitShowAndDesc() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products?page=0&&size=2&&direction=DESC"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productCode",Matchers.is("S10_1680")))
                .andExpect(jsonPath("$[1].productCode",Matchers.is("S10_1679")));

        Page<Product> pages = productRepository.findAll(
                PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "productCode")));
        assertEquals(2,pages.getTotalPages());
        assertEquals(2,pages.getSize());
        assertArrayEquals(
                new String[] { "S10_1680", "S10_1679"},
                pages.stream().map(Product::getProductCode).toArray(String[]::new));
    }

    @Test
    void shouldGetAllProductLine() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productLine",Matchers.is("Motorcycles")))
                .andExpect(jsonPath("$[1].productLine",Matchers.is("Planes")));
    }

    @Test
    void shouldGetAllProductLinesDESC() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines?direction=DESC"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[1].productLine",Matchers.is("Motorcycles")))
                .andExpect(jsonPath("$[0].productLine",Matchers.is("Planes")));
    }

    @Test
    void shouldGetAllProductLinesSplitShow() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/products?page=0&&size=1"))
                .andExpect(status().is(200));

        Page<ProductLine> pages = productLineRepository.findAll(
                PageRequest.of(1, 1, Sort.by(Sort.Direction.ASC, "productLine")));
        assertEquals(2,pages.getTotalPages());
        assertEquals(1,pages.getSize());
        assertArrayEquals(
                new String[] { "Planes" },
                pages.stream().map(ProductLine::getProductLine).toArray(String[]::new));
    }

    @Test
    void shouldGetAllProductLinesSplitShowAndDesc() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines?page=0&&size=1&&direction=DESC"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productLine",Matchers.is("Planes")));

        Page<ProductLine> pages = productLineRepository.findAll(
                PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "productLine")));
        assertEquals(2,pages.getTotalPages());
        assertEquals(1,pages.getSize());
        assertArrayEquals(
                new String[] {"Planes"},
                pages.stream().map(ProductLine::getProductLine).toArray(String[]::new));
    }

    @Test
    void shouldGetAllProductLinesSplitShowAndDescByDescription() throws Exception {
        CreateRecordsToProducts();
        getMockMvc().perform(get("/api/product-lines?page=0&&size=2&&direction=DESC&&sort=textDescription"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].productLine",Matchers.is("Motorcycles")))
                .andExpect(jsonPath("$[1].productLine",Matchers.is("Planes")));
    }

    @Test
    void shouldCreateProduct() throws Exception {
        final Product product = new Product();
        final ProductLine productLine = new ProductLine();
        productLine.setProductLine("Motorcycles");
        productLine.setTextDescription("Wur motorcycles are state of the art replicas of classic as well as contemporary motorcycle legends such as Harley Davidson, Ducati and Vespa. Models contain stunning details such as official logos, rotating wheels, working kickstand, front suspension, gear-shift lever, footbrake lever, and drive chain. Materials used include diecast and plastic. The models range in size from 1:10 to 1:50 scale and include numerous limited edition and several out-of-production vehicles. All models come fully assembled and ready for display in the home or office. Most include a certificate of authenticity.");
        productLineRepository.saveAndFlush(productLine);
        product.setProductCode("S10_1678");
        product.setProductName("1969 Harley Davidson Ultimate Chopper");
        product.setProductLine(productLine);
        product.setProductScale("1:10");
        product.setProductVendor("Min Lin Diecast");
        product.setProductDescription("This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.");
        product.setQuantityInStock((short)7933);
        product.setBuyPrice(BigDecimal.valueOf(48.81));
        product.setMSRP(BigDecimal.valueOf(95.70));

        final GetProductRequest request = new GetProductRequest(product.getProductCode(), product.getProductName(),
                product.getProductLine(), product.getProductScale(), product.getProductVendor(), product.getProductDescription(),
                product.getQuantityInStock(), product.getBuyPrice(), product.getMSRP());
        final String json = new ObjectMapper().writeValueAsString(request);
        getMockMvc().perform(post("/api/products")
                .content(json).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(201));
        assertTrue(productRepository.findProductByProductCode(product.getProductCode()).isPresent());
    }

    public void CreateRecordsToProducts(){
        final Product product = new Product();
        final ProductLine productLine = new ProductLine();
        productLine.setProductLine("Motorcycles");
        productLine.setTextDescription("Wur motorcycles are state of the art replicas of classic as well as contemporary motorcycle legends such as Harley Davidson, Ducati and Vespa. Models contain stunning details such as official logos, rotating wheels, working kickstand, front suspension, gear-shift lever, footbrake lever, and drive chain. Materials used include diecast and plastic. The models range in size from 1:10 to 1:50 scale and include numerous limited edition and several out-of-production vehicles. All models come fully assembled and ready for display in the home or office. Most include a certificate of authenticity.");
        productLineRepository.saveAndFlush(productLine);
        product.setProductCode("S10_1678");
        product.setProductName("1969 Harley Davidson Ultimate Chopper");
        product.setProductLine(productLine);
        product.setProductScale("1:10");
        product.setProductVendor("Min Lin Diecast");
        product.setProductDescription("This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.");
        product.setQuantityInStock((short)7933);
        product.setBuyPrice(BigDecimal.valueOf(48.81));
        product.setMSRP(BigDecimal.valueOf(95.70));
        productRepository.saveAndFlush(product);

        final Product product1 = new Product();
        product1.setProductCode("S10_1679");
        product1.setProductName("1969 Harley Davidson Ultimate Chopper");
        product1.setProductLine(productLine);
        product1.setProductScale("1:10");
        product1.setProductVendor("Min Lin Diecast");
        product1.setProductDescription("This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.");
        product1.setQuantityInStock((short)7933);
        product1.setBuyPrice(BigDecimal.valueOf(48.81));
        product1.setMSRP(BigDecimal.valueOf(95.70));
        productRepository.saveAndFlush(product1);

        final Product product2 = new Product();
        final ProductLine productLine1 = new ProductLine();
        product2.setProductCode("S10_1680");
        product2.setProductName("1969 Harley Davidson Ultimate Chopper");
        productLine1.setProductLine("Planes");
        productLine1.setTextDescription("Unique, diecast airplane and helicopter replicas suitable for collections, as well as home, office or classroom decorations. Models contain stunning details such as official logos and insignias, rotating jet engines and propellers, retractable wheels, and so on. Most come fully assembled and with a certificate of authenticity from their manufacturers.");
        productLineRepository.saveAndFlush(productLine1);
        product2.setProductLine(productLine1);
        product2.setProductScale("1:10");
        product2.setProductVendor("Min Lin Diecast");
        product2.setProductDescription("This replica features working kickstand, front suspension, gear-shift lever, footbrake lever, drive chain, wheels and steering. All parts are particularly delicate due to their precise scale and require special care and attention.");
        product2.setQuantityInStock((short)7933);
        product2.setBuyPrice(BigDecimal.valueOf(48.81));
        product2.setMSRP(BigDecimal.valueOf(95.70));
        productRepository.saveAndFlush(product2);

    }
}
